# [《剑指 Offer（第 2 版）》系列](https://leetcode-cn.com/problemset/lcof/)
本书精选谷歌、微软等知名IT企业的典型面试题，系统地总结了如何在面试时写出高质量代码，如何优化代码效率，以及分析、解决难题的常用方法。

## [题解](https://github.com/doocs/leetcode/tree/master/lcci)

```
.
├── README.md
├── README_TEMPLATE.md
├── 面试题03. 数组中重复的数字
│   ├── README.md
│   └── Solution.py
├── 面试题04. 二维数组中的查找
│   ├── README.md
│   ├── Solution.java
│   └── Solution.py
├── 面试题05. 替换空格
│   ├── README.md
│   ├── Solution.java
│   └── Solution.py
├── 面试题06. 从尾到头打印链表
│   ├── README.md
│   ├── Solution.java
│   └── Solution.py
├── 面试题07. 重建二叉树
│   ├── README.md
│   ├── Solution.java
│   └── Solution.py
├── 面试题09. 用两个栈实现队列
│   ├── README.md
│   ├── Solution.java
│   └── Solution.py
├── 面试题10- I. 斐波那契数列
│   ├── README.md
│   ├── Solution.java
│   └── Solution.py
├── 面试题10- II. 青蛙跳台阶问题
│   ├── README.md
│   ├── Solution.java
│   └── Solution.py
├── 面试题11. 旋转数组的最小数字
│   ├── README.md
│   ├── Solution.java
│   └── Solution.py
├── 面试题18. 删除链表的节点
│   ├── README.md
│   ├── Solution.java
│   └── Solution.py
├── 面试题22. 链表中倒数第k个节点
│   ├── README.md
│   ├── Solution.java
│   └── Solution.py
├── 面试题24. 反转链表
│   ├── README.md
│   ├── Solution.java
│   └── Solution.py
├── 面试题25. 合并两个排序的链表
│   ├── README.md
│   ├── Solution.java
│   └── Solution.py
└── 面试题26. 树的子结构
    ├── README.md
    ├── Solution.java
    └── Solution.py
```

## 版权
著作权归 [GitHub 开源社区 Doocs](https://github.com/doocs) 所有，商业转载请联系 [@yanglbme](mailto:contact@yanglibin.info) 授权，非商业转载请注明出处。